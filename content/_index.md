+++
title = "Home"
outputs = ["Reveal"]
[logo]
src = "Logo-MAX-couleur.png"
alt = "Max logo"
margin = 0.2
+++

{{<slide background-image="background.jpg" background-opacity="0.2" background-position="0% 0%">}}

<h1>
    <span class="blue">A</span><span class="yellow">p</span><span class="red">a</span><span class="green">c</span><span class="blue">h</span><span class="yellow">e </span><span class="red">C</span><span class="green">a</span><span class="blue">m</span><span class="yellow">e</span><span class="red">l</span><span class="green">, </span><span class="blue">l</span><span class="yellow">e </span><span class="red">f</span><span class="green">r</span><span class="blue">a</span><span class="yellow">m</span><span class="red">e</span><span class="green">w</span><span class="blue">o</span><span class="yellow">r</span><span class="red">k </span><span class="green">p</span><span class="blue">o</span><span class="yellow">u</span><span class="red">r </span><span class="green">d</span><span class="blue">e</span><span class="yellow">v</span><span class="red">e</span><span class="green">n</span><span class="blue">i</span><span class="yellow">r </span><span class="red">u</span><span class="green">n </span><span class="blue">p</span><span class="yellow">l</span><span class="red">o</span><span class="green">m</span><span class="blue">b</span><span class="yellow">i</span><span class="red">e</span><span class="green">r </span><span class="blue">s</span><span class="yellow">u</span><span class="red">p</span><span class="green">e</span><span class="blue">r </span><span class="yellow">s</span><span class="red">t</span><span class="green">a</span><span class="blue">r</span>
</h1>

<br/>

<div>
    <img style="border-radius:50%; width:3em;" src="mat_souch.jpg">
    <div style="display:flex; justify-content:center; align-items:center;">
        <img src="twitter-icon.png" style="border:none; background:transparent; margin-right:15px;" /><a href="https://twitter.com/Gasouch">@Gasouch</a>
    </div>
</div>

{{% note %}}
* Moi : Développeur Back, Java

* Envoyé en mission dans une équipe de plombiers pour Yves Rocher pendant près de 5 ans

* Découverte des flux d'intégration

* Objectifs : RETEX / parler de flux d'intégration / parler de Camel

* Qui a déjà fait des flux d'intégration ? Utilisé Camel ?
{{% /note %}}

---

{{<slide background-image="background.jpg" background-opacity="0.2" background-position="0% 0%">}}

{{% section %}}

<h2>
    <span class="blue">L</span><span class="yellow">e</span><span class="red">s </span><span class="green">f</span><span class="blue">l</span><span class="yellow">u</span><span class="red">x </span><span class="green">d</span><span class="blue">'</span><span class="yellow">i</span><span class="red">n</span><span class="green">t</span><span class="blue">e</span><span class="yellow">g</span><span class="red">r</span><span class="green">a</span><span class="blue">t</span><span class="yellow">i</span><span class="red">o</span><span class="green">n </span><span class="blue">?</span>
</h2>

{{% note %}}
* Pour comprendre ce que sont les flux d'intégration, intéressons-nous à un exemple concret : Une entreprise qui vend des biens via une plateforme de E-Commerce
{{% /note %}}

---

![ecommerce-supply-chain](ecommerce-supply-chain.jpg)

{{% note %}}
* A chaque étape de la chaine, on a des systèmes informatiques, basés sur des technologies très disparates (site e-commerce, Data Warehouse pour le décisionnel, grands systèmes IBM AS400/Cobol, BDD Oracle, micro-services REST)

* Les flux d'intégration, ça sert à connecter tous ces systèmes, à automatiser tous les process. **On intègre des systèmes disparates** via des flux de donnée.
{{% /note %}}

---

<h3>
    <span class="blue">C</span><span class="yellow">o</span><span class="red">n</span><span class="green">t</span><span class="blue">r</span><span class="yellow">a</span><span class="red">i</span><span class="green">n</span><span class="blue">t</span><span class="yellow">e</span><span class="red">s</span>
</h3>

* Multiples technos
* Multiples protocoles

<br/>
<br/>
<br/>


<h3>
    <span class="blue">O</span><span class="yellow">b</span><span class="red">j</span><span class="green">e</span><span class="blue">c</span><span class="yellow">t</span><span class="red">i</span><span class="green">f</span>
</h3>

Améliorer la communication entre tous ces sytèmes informatiques via des intégrations :
* Temps réel (au lieu des traditionnels batchs de nuit)
* Asynchrones
* Résilients (Rejeu possible des erreurs)

---

![basic-flow-sample-graph](basic-flow-sample-graph.png)

{{% note %}}
* Pour faire transiter des flux de données entre 2 systèmes, on code un flux d'intégration

* Le flux comprend :
  - un connecteur qui consomme la donnée mise à disposition par le système A
  - une route de traitement de la donnée (enrichissement, filtrage, transformation...)
  - un connecteur qui envoie la donnée vers le système B

* Les connecteurs et les systèmes communiquent via le même protocole
{{% /note %}}

---

🔗 https://www.enterpriseintegrationpatterns.com/toc.html

{{% note %}}
* Ces flux mettent en évidence différents patterns (split de données, agrégations, transformations)
{{% /note %}}

---

<div style="display: flex; justify-content: space-evenly; align-items: center;">
    <div>
        <img style="border-radius:50%; width:3em;" src="bobby-woolf.jpg">
        <div style="display:flex; justify-content:center; align-items:center;">
            <img src="twitter-icon.png" style="border:none; background:transparent; margin-right:15px;" /><a href="https://twitter.com/bobby_woolf">@bobby_woolf</a>
        </div>
    </div>
    <div>
        <img src="enterprise-integration-patterns-book.jpg">
    </div>
</div>


{{% note %}}
* Bobby Woolf (IBM) a écrit un bouquin en 2003 regroupant tous les principaux patterns d'intégration.

* Ce bouquin fait désormais office de référence.
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="background.jpg" background-opacity="0.2" background-position="0% 0%">}}

{{% section %}}

<h2>
    <span class="blue">A</span><span class="yellow">p</span><span class="red">a</span><span class="green">c</span><span class="blue">h</span><span class="yellow">e </span><span class="red">C</span><span class="green">a</span><span class="blue">m</span><span class="yellow">e</span><span class="red">l </span><span class="green">?</span>
</h2>

<br/>

<div class="fragment">

<div style="display: flex; justify-content: space-around; align-items: center; flex-wrap: wrap;">
    <div style="width:75%;">
        <div style="display: flex; justify-content: center; align-items: center;">
            <img src="apache-camel-logo.png" style="border:none; box-shadow: none; width:40px; margin-right: 10px;"/><a href="https://camel.apache.org/">camel.apache.org</a>
        </div>
        <img src="what-is-camel.png"/>
    </div>
    <img src="camel-github-stats.png" style="width:22%;"/>
</div>

</div>

{{% note %}}
* Framework Open-Source porté principalement par Redhat, codé en java

* Grosse communauté

* Camel offre sur étagère tout un tas de composants :
  - se connecter à à peu près tout ce qui existe (très facile de rajouter un connecteur sur le repo)
  - protocoles, ressources cloud

-> Camel permet de ne pas ré-inventer la roue
{{% /note %}}

---

![camel-eip](camel-eip.png)

🔗 https://camel.apache.org/components

{{% note %}}
* Tous les patterns d'intégration du bouquion sont adressés par le framework Camel (même nomenclature, mêmes graphiques)

* Le moteur Camel offre un moyen de coder les routes des flux d'intégration (Il va également les piloter, les lancer, les arrêter)

* Camel se veut léger et extensible, on intègre uniquement les composants que l'on a besoin (voir lien)
{{% /note %}}

---

<h2>
    <span class="blue">R</span><span class="yellow">u</span><span class="red">n</span><span class="green">t</span><span class="blue">i</span><span class="yellow">m</span><span class="red">e</span><span class="green">s </span><span class="blue">C</span><span class="yellow">a</span><span class="red">m</span><span class="green">e</span><span class="blue">l</span>
</h2>

<div style="display: flex; justify-content: space-around; align-items: center; flex-wrap: wrap;">
    <img src="camel-standalone.png" style="border:none; box-shadow: none; width:300px;"/>
    <img src="spring-boot-logo.png" style="border:none; box-shadow: none; width:300px;"/>
    <img src="quarkus-logo.png" style="border:none; box-shadow: none; width:200px;"/>
    <img src="karaf-logo.webp" style="border:none; box-shadow: none; width:200px;"/>
    <img src="wildfly-logo.png" style="border:none; box-shadow: none; width:300px;"/>
    <img src="vertx-logo.png" style="border:none; box-shadow: none; width:200px;"/>
    <img src="tomcat-logo.png" style="border:none; box-shadow: none; width:150px;"/>
    <img src="kafka-logo.png" style="border:none; box-shadow: none; width:200px;"/>
    <img src="knative-logo.png" style="border:none; box-shadow: none; width:150px;"/>
    <img src="microprofile-logo.png" style="border:none; box-shadow: none; width:300px;"/>
</div>

---

<h2>
    <span class="blue">D</span><span class="yellow">o</span><span class="red">m</span><span class="green">a</span><span class="blue">i</span><span class="yellow">n </span><span class="red">S</span><span class="green">p</span><span class="blue">e</span><span class="yellow">c</span><span class="red">i</span><span class="green">f</span><span class="blue">i</span><span class="yellow">c </span><span class="red">L</span><span class="green">a</span><span class="blue">n</span><span class="yellow">g</span><span class="red">u</span><span class="green">a</span><span class="blue">g</span><span class="yellow">e </span><span class="red">(</span><span class="green">D</span><span class="blue">S</span><span class="yellow">L</span><span class="red">)</span>
</h2>

<br/>
<br/>

* **Java DSL** : Java based DSL using the fluent builder style.
* **XML DSL** : XML based DSL in Camel XML files only.
* **Spring XML** : XML based DSL in classic Spring XML files.

{{% note %}}
* Il y a plusieurs façons d'écrire du Camel
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="background-mario-thinking.jpg" background-opacity="0.2" background-position="0% 0%">}}

{{% section %}}

```java{|3-6|10|13,15|}
import org.apache.camel.builder.RouteBuilder;

public class MyRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        // here is a sample which processes the input files
        // (leaving them in place - see the 'noop' flag)
        // then performs content based routing on the message using XPath
        from("file:src/data?noop=true")
            .choice()
                .when(xpath("/person/city = 'London'"))
                    .to("file:target/messages/uk")
                .otherwise()
                    .to("file:target/messages/others");
    }
}
```

{{% note %}}
* Un rapide coup d'oeil sur le code

* On étend la classe `RouteBuilder`

* On a des mots clés qui reviennent à chaque fois : `from()` et `to()` 

* protocole utilisé ici pour consommer la donnée : `file`, pareil pour la sortie

* **le DSL ça fonctionne comme les pipes Linux, la sortie d'une étape est l'entrée de l'étape d'après**
{{% /note %}}

---

<h2>
    <span class="blue">D</span><span class="yellow">e</span><span class="red">m</span><span class="green">o </span><span class="blue">T</span><span class="yellow">i</span><span class="red">m</span><span class="green">e </span><span class="blue">!</span>
</h2>

---

<h2>
    <span class="blue">C</span><span class="yellow">a</span><span class="red">m</span><span class="green">e</span><span class="blue">l </span><span class="yellow">E</span><span class="red">x</span><span class="green">c</span><span class="blue">h</span><span class="yellow">a</span><span class="red">n</span><span class="green">g</span><span class="blue">e</span><span class="yellow">s</span>
</h2>

<div style="flex-direction: column; justify-content: center;">
    <img src="camel-exchange.webp" style="width: 350px;"/>
    <img src="message-flow-in-route.png"/>
</div>

{{% note %}}
* MEP : Message Exchange Pattern : `InOnly` ou `InOut`

* Properties : Durée de vie de l'exchange, pour ajouter des infos en dehors du message

* Headers : Transmis avec le message (http, Queuing)
{{% /note %}}

---

```java
@Override
public void configure() throws Exception {
    rest("/say")
        .post("/hello").consumes("application/json").to("direct:hello")
        .get("/bye").to("direct:bye")
        .post("/bye").consumes("application/json").to("direct:bye")
    ;

    from("direct:hello")
        .setBody("Hello World")
    ;

    from("direct:bye")
        .setBody("Bye World")
    ;
}
```

| Base path | Uri template | Verb | Consumes         |
|-----------|--------------|------|------------------|
| /say      | /hello       | POST | application/json |
| /say      | /bye         | GET  | all              |
| /say      | /bye         | POST | application/json |


{{% note %}}
* Dans cet exemple on a implémenté un Webservice REST comme décrit dans le tableau

* necessite d'importer la dépendance `camel-rest` et `camel-log`
{{% /note %}}

---

```java
@Override
public void configure() throws Exception {

    from("direct:jsonInput")
        .unmarshal().json(JsonLibrary.Jackson, InputType.class)
        .bean(MappingBean.class, "doSomething")
        .marshal().json(JsonLibrary.Jackson)
        .to("http://targetSystem.com")
    ;

}
```

```java
public class MappingBean {

    public TargetType doSomething(InputType input) {
        return new TargetType(input.getUsefullField());
    }

}
```

{{% note %}}
* Dans cet exemple on a implémenté un bean de Mapping écrit en Java pur

* Notez bien les commandes pour sérialiser/désérialiser avec `camel-jackson`
{{% /note %}}


---

<h2>
    <span class="blue">c</span><span class="yellow">a</span><span class="red">m</span><span class="green">e</span><span class="blue">l</span><span class="yellow">-</span><span class="red">c</span><span class="green">r</span><span class="blue">o</span><span class="yellow">n</span>
</h2>

```java
from("cron:tab?schedule=0/10 * * * *")
    .setBody().constant("event")
    .log("${body}")
;
```

{{% note %}}
* Toutes les 10min tous les jours

* composant `camel-cron`
{{% /note %}}

---

<h2>
    <span class="blue">I</span><span class="yellow">n</span><span class="red">t</span><span class="green">e</span><span class="blue">r</span><span class="yellow">n</span><span class="red">a</span><span class="green">l </span><span class="blue">e</span><span class="yellow">n</span><span class="red">d</span><span class="green">p</span><span class="blue">o</span><span class="yellow">i</span><span class="red">n</span><span class="green">t</span><span class="blue">s</span>
</h2>

* `direct` : Synchrone (même thread)

* `seda` : Asynchrone (file SEDA, nouveau thread), même contexte Camel

* `vm` :  Asynchrone (file SEDA, nouveau thread), même JVM

{{% note %}}
* `direct` : pratique pour factoriser du code

* diff entre `seda` & `vm` : la visibilité
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="background-futur.jpg" background-opacity="0.2" background-position="0% 0%">}}

{{% section %}}

<h2>
    <span class="blue">A</span><span class="yellow">u</span><span class="red">t</span><span class="green">r</span><span class="blue">e</span><span class="yellow">s </span><span class="red">p</span><span class="green">r</span><span class="blue">o</span><span class="yellow">j</span><span class="red">e</span><span class="green">t</span><span class="blue">s</span>
</h2>

---

<h2>
    <span class="blue">C</span><span class="yellow">a</span><span class="red">m</span><span class="green">e</span><span class="blue">l </span><span class="yellow">Q</span><span class="red">u</span><span class="green">a</span><span class="blue">r</span><span class="yellow">k</span><span class="red">u</span><span class="green">s</span>
</h2>

<br/>

🔗 https://quarkus.io/guides/camel 

🔗 https://github.com/apache/camel-quarkus

<br/>

⬇️ Plus petites images Docker

⬆️ Démarrage plus rapide 

⬇️ Empreinte RAM plus petite

🤩 Depuis Camel 3.9 : DSL Kotlin, Groovy, YAML et JavaScript

{{% note %}}
* Evolutions dans le coeur de Camel pour que Camel aie tout ce qu'il faut au buildtime et non au runtime

* Développement d'extensions Quarkus pour tous les composants Camel (301 aujourd'hui)

* Un démarrage plus rapide permet de scale up plus rapidement, et de scale down le temps où le flux est inactif
{{% /note %}}

---

<h2>
    <span class="blue">C</span><span class="yellow">a</span><span class="red">m</span><span class="green">e</span><span class="blue">l </span><span class="yellow">K</span>
</h2>

<br/>

🔗 https://camel.apache.org/camel-k

**Serverless Cloud Native on Kubernetes**

<div style="display: flex; justify-content: space-around; align-items: center;">
    <img src="camel-k-architecture.png" style="width: 60%;"/>
    <img src="kamel-sample.png" style="width: 35%"/>
</div>

{{% note %}}
* check status : `kamel get`

* get logs: `kamel logs hello`
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="background-futur.jpg" background-opacity="0.2" background-position="0% 0%">}}

<h2>
    <span class="blue">B</span><span class="yellow">i</span><span class="red">l</span><span class="green">a</span><span class="blue">n</span>
</h2>

<br/>

👍 Open-source, grosse communauté

👍 Focus sur la code métier, Camel gère la partie technique

👍 Framework extensible, connecteurs interchangeables

<br/>

👎 Montée en compétence

👎 Tests pas toujours simples

👎 Composants parfois limités

{{% note %}}
* Très facile de changer de fournisseur de cloud par exemple

* Il m'est arrivé de devoir recoder un composant pour avoir plus de features
{{% /note %}}

---

{{<slide background-image="background.jpg" background-opacity="0.2" background-position="0% 0%">}}

<h2>
    <span class="blue">G</span><span class="yellow">a</span><span class="red">m</span><span class="green">e </span><span class="blue">O</span><span class="yellow">v</span><span class="red">e</span><span class="green">r</span>
</h2>

<img src="mario-dabb.png" style="border:none; box-shadow: none; width:200px;"/>

{{% note %}}
* Des questions ?
{{% /note %}}

